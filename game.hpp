#pragma once

#include "cpu.hpp"

class SDL_Window;
class SDL_Surface;
class SDL_Renderer;
class Mix_Chunk;

class Game
{
public:
  void input();
  void destroy();
  void setup();
  void blank();
  void clear();
  void beep();
	void draw_gfx();
	void draw_rectangle(int ax, int ay);
  
  SDL_Window* window = nullptr;
  SDL_Surface* screen_surface = nullptr;
  SDL_Renderer* renderer = nullptr;
  Mix_Chunk* snd_boop = nullptr;
  cpu_t cpu;
  bool running = true;
  
};

extern Game gGame;
