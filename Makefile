#OBJS specifies which files to compile as part of the project
OBJS = main.cpp cpu.cpp game.cpp

#OBJ_NAME specifies the name of our exectuable
OBJ_NAME = cpu

#This is the target that compiles our executable
all : $(OBJS)
	g++ $(OBJS) -lSDL2 -lSDL2_mixer -o $(OBJ_NAME) -g -Wall -Wextra -Wformat-nonliteral -Wcast-align -Wpointer-arith \
-Wmissing-declarations -Winline -Wundef \
-Wcast-qual -Wshadow -Wwrite-strings -Wno-unused-parameter \
-Wfloat-equal -pedantic
