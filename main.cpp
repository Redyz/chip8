#include <stdio.h>
#include "cpu.hpp"
#include "game.hpp"
#include <SDL2/SDL.h>
Game gGame;
int main(int argc, char** argv)
{
  gGame.setup();

  while(gGame.running)
  {
    gGame.cpu.step();
    
    gGame.input();
    if(gGame.cpu.draw_flag)
    {
      gGame.blank();
      gGame.draw_gfx();
    }
    
    SDL_Delay( 2 );
  }

  gGame.destroy();
  
  return 0;
}
