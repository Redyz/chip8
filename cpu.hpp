#pragma once

#include <iostream>
#include <string>
#include <bitset>
#include <sstream>
#include <iomanip>

#define HEIGHT 32
#define WIDTH 64
#define SCALE 10
#define S_HEIGHT HEIGHT*SCALE
#define S_WIDTH WIDTH*SCALE
#define MEM_OFFSET 512
#define MEM_SIZE 4096

#define LOG(X) std::cout << X << std::endl;

struct cpu_t
{
  void initialize(); 
  void load_game(std::string filename); 
  void step();
  void mem_dump();
  void stack_dump();
  void reg_dump();
  void context_dump();
  int await_keypress();
  
  bool draw_flag = true;
  unsigned char memory[MEM_SIZE];
  unsigned char V[16];
  unsigned short I, pc;
  unsigned char gfx[HEIGHT*WIDTH];
  unsigned char delay_timer, sound_timer;
  unsigned short stack[16], sp;
  unsigned char key[16];
};

//template< typename T >
inline std::string int_to_string(short i)
{
  std::stringstream stream;
  stream << std::setfill ('0') << std::setw(3) << std::hex << i;
  return stream.str();
}      

inline std::string dump(unsigned short opcode)
{
//   std::cout << opcode << std::endl;
  unsigned char half = opcode >> 8, other_half = opcode;
  std::stringstream stream;
  stream << std::uppercase << std::hex << ((int)half) << (int)other_half; 
  return stream.str();
}
