#include "cpu.hpp"
#include <iomanip>
#include <istream>
#include <fstream>                                                                                                                                                                                      
#include "game.hpp"
#include <cstring>
#include <SDL2/SDL_timer.h>

unsigned char chip8_fontset[80] =
{ 
  0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
  0x20, 0x60, 0x20, 0x20, 0x70, // 1
  0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
  0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
  0x90, 0x90, 0xF0, 0x10, 0x10, // 4
  0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
  0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
  0xF0, 0x10, 0x20, 0x40, 0x40, // 7
  0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
  0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
  0xF0, 0x90, 0xF0, 0x90, 0x90, // A
  0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
  0xF0, 0x80, 0x80, 0x80, 0xF0, // C
  0xE0, 0x90, 0x90, 0x90, 0xE0, // D
  0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
  0xF0, 0x80, 0xF0, 0x80, 0x80  // F
};

void cpu_t::initialize()
{
  pc = 0x200;
  I = 0;
  sp = 0;
  
  load_game("games/VERS");
  
  // load font
  for(int i = 0; i < 80; ++i)
    memory[i] = chip8_fontset[i];
}

void cpu_t::load_game(std::string filename)
{
  
  memset(memory, 0, MEM_SIZE);
  int size = 0;
  {
    std::ifstream t(filename, std::ios::ate | std::ios::binary); 
    size = t.tellg();
    t.close();
  }
  
  std::ifstream f(filename, std::ios::in | std::ios::binary);
  
  char program[size];
  f.read(program, size);
  f.close();
  
  // load program
  int i = 0;
  for(unsigned char current : program)
  {
    std::string temp = int_to_string((short)current);
    temp = temp.substr(1, temp.size());
    if(temp.size() >= 2)
    {
      i++;
      std::cout << temp << (i%2 == 0 ? " " : "");
      if(i >= 28)
      {
        i = 0;
        std::cout << std::endl;
      }
    }
  }
  
  for(int j = 0; j < size; j++)
  {
    memory[512+j] = program[j]; 
  }
  
  std::cout << std::endl;
  std::cout << "END OF PROGRAM DUMP" << std::endl;
  
  printf("Parsed game file for %i bytes\n", size);
  
}

void cpu_t::mem_dump()
{
  int i = 0, t = 0;
  for(unsigned char current : memory)
  {
    std::string temp = int_to_string((short)current);
    temp = temp.substr(1, temp.size());
    if(temp.size() >= 2)
    {
      i++;
      std::cout << temp << (i%2 == 0 ? " " : "");
      if(i >= 28)
      {
        t+=i;
        i = 0;
        std::cout << "\n>> " << t << std::endl;
      }
    }
  }
}

void cpu_t::reg_dump()
{
  for(uint i = 0; i < sizeof(V)/sizeof(V[0]); i++)
  {
    std::cout << "V" << i << ": " << (int)V[i] << " ";
  }
  std::cout << std::endl;
}

void cpu_t::stack_dump()
{
  for(uint i = 0; i < sizeof(stack)/sizeof(stack[0]); i++)
  {
    printf("S%i: %i\n", i, stack[i]);
  }
}

void cpu_t::context_dump()
{
  for(int i = -5; i < 5; i+=2)
  {
    unsigned short t = memory[pc+i] << 8 | memory[pc + i + 1];
    std::cout << dump(t) << " ";
  }
}

int cpu_t::await_keypress()
{
  bool done = false;
  int key_pressed = -1;
  while(!done)
  {
    for(int i = 0; i < 16; i++)
    {
      if(key[i])
      {
        done = true;
        key_pressed = i;
      }
    }
    if(done)
      break;
    
    SDL_Delay(5);
    gGame.input();
  }
  
  return key_pressed;
}

void cpu_t::step()
{
  unsigned short op = memory[pc] << 8 | memory[pc + 1];
  unsigned short arg = 0, arg1 = 0;
  //std::cout << dump(op) << std::endl;
//   std::cout << "===============" << std::endl;;
//   reg_dump();
//   stack_dump();
  std::stringstream stre;
  for(int i = 0; i < 16; i++)
    stre << std::setw(2) << "V" << i << "[" << (int)V[i] << "] ";
  
  std::string stack_size(sp, '.');
  std::cout << stack_size << std::hex << pc << 
    std::dec <<  " " << int_to_string(op)
    << " -- " << " I: " <<  std::hex << I
    << " *I: " << (int)memory[I] <<  " " << (int)memory[I+1] << " " << (int)memory[I+2] << " " << stre.str() << std::endl;
  
  int keyPressed = 0;
  for(int h = 0; h < 16; h++)
  {
    keyPressed += (key[h] == true);
  }
   
  switch(op & 0xF000)
  {
    case 0x0000:
    {
      switch(op & 0x00FF)
      {
        case 0x00E0: gGame.clear(); pc+=2; break;
        case 0x00EE: pc=stack[(--sp)&0x0F]+2; break;
        default: LOG("UNKNOWN OP"); break;
      }
    }
    break;
    case 0x1000: pc = op & 0x0FFF; break;
    case 0x2000: // OK
    {
      stack[(sp++)&0x0F] = pc;
      pc = op & 0x0FFF;
    }
    break;
    case 0x3000:
    {
      arg = (op & 0x0F00) >> 8;
      arg1 = op & 0x00FF;
      if(V[arg] == arg1)
        pc += 4; // skip next inst.
      else
        pc += 2;
    }
    break;
    case 0x4000:
    {
      arg = (op & 0x0F00) >> 8;
      arg1 = op & 0x00FF;
      if(V[arg] != arg1)
        pc += 4; // skip next inst.
      else
        pc += 2;
    }
    break;
    case 0x5000:
    {
      arg = (op & 0x0F00) >> 8;
      arg1 = (op & 0x00F0) >> 4;
      if(V[arg] == V[arg1])
        pc += 4; // skip next inst.
      else
        pc += 2;
    }
    break;
    case 0x6000:
    {
      arg = (op & 0x0F00) >> 8;
      arg1 = op & 0x00FF;
      V[arg] = arg1;
      pc += 2;
    }
    break;
    case 0x7000:
    {
      arg = (op & 0x0F00) >> 8;
      arg1 = op & 0x00FF;
      V[arg] += arg1;
      pc += 2;
    }
    break;
    case 0x8000:
    {
      arg = (op & 0x0F00) >> 8;
      arg1 = (op & 0x00F0) >> 4;
      switch(op & 0x000F)
      {
        case 0x0000: V[arg] = V[arg1]; break;
        case 0x0001: V[arg] = V[arg] | V[arg1]; break;
        case 0x0002: V[arg] = V[arg] & V[arg1]; break;
        case 0x0003: V[arg] = V[arg] ^ V[arg1]; break;
        case 0x0004: V[15] = ((int)V[arg] + (int)V[arg1] > 254); V[arg] += V[arg1]; break;
        case 0x0005: V[15] = !((int)V[arg] - (int)V[arg1] < 0); V[arg] -= V[arg1]; break;
        case 0x0006: V[15] = V[arg] & 0x0001; V[arg] >>= 1; break;
        case 0x0007: V[15] = !((int)V[arg1] - (int)V[arg] < 0); V[arg] = V[arg1] - V[arg]; break;
        case 0x000E: V[15] = V[arg] & 0x8000; V[arg] <<= 1; break;
      }
      
      pc += 2;
    }
    break;
    case 0x9000:
    {
      arg = (op & 0x0F00) >> 8;
      arg1 = (op & 0x00F0) >> 4;
      if(V[arg] != V[arg1])
        pc += 4; // skip next inst.
      else
        pc += 2;
    }
    break;
    case 0xA000:
    {
      I = op & 0x0FFF;
      pc+=2;
    }
    break;
    case 0xB000:
    {
      arg = (op & 0x0FFF);
      pc=V[0]+arg;
    }
    break;
    case 0xC000:
    {
      arg = (op & 0x0F00) >> 8;
      arg1 = (op & 0x00FF);
      V[arg] = (rand()%255)&arg1;
      pc+=2;
    }
    break;
    case 0xD000:
    {
      unsigned short x = V[(op & 0x0F00) >> 8];
      unsigned short y = V[(op & 0x00F0) >> 4];
      unsigned short height = op & 0x000F;
      unsigned short pixel;
      V[0xF] = 0;
      for (int yline = 0; yline < height; yline++)
      {
        pixel = memory[I + yline];
        for(int xline = 0; xline < 8; xline++)
        {
          if((pixel & (0x80 >> xline)) != 0)
          {
            if(gfx[((x + xline)%WIDTH + (((y + yline)%HEIGHT)* 64))] == 1)
              V[0xF] = 1;                                 
            gfx[(x + xline)%WIDTH + (((y + yline)%HEIGHT) * 64)] ^= 1;
          }
        }
      }
      draw_flag = true;
      pc+=2;
    }
    break;
    case 0xE000:
    {
      arg = (op & 0x0F00) >> 8;
      switch(op & 0x00F0)
      {
        case 0x0090:
        {
          if(key[V[arg]])
          {
            pc += 4;
          }else
            pc += 2;
        }
        break;
        case 0x00A0:
        {
          if(!key[V[arg]])
          {
            pc += 4;
          }else
            pc += 2;
        }
        break;
        
        default: LOG("UNKNOWN OPCODE");
      }
    }
    break;
    case 0xF000:
    {
      arg = (op & 0x0F00) >> 8;
      switch(op & 0x00FF)
      {
        case 0x0007: V[arg] = delay_timer; break;
        case 0x000A: { int key_pressed = await_keypress(); V[arg] = key_pressed;} break;
        case 0x0015: delay_timer = V[arg]; break;
        case 0x0018: sound_timer = V[arg]; break;
        case 0x001E: I += V[arg]; break;
        case 0x0033:
        {
          memory[I]     = V[(op & 0x0F00) >> 8] / 100;
          memory[I + 1] = (V[(op & 0x0F00) >> 8] / 10) % 10;
          memory[I + 2] = (V[(op & 0x0F00) >> 8] % 100) % 10;
          std::cout << "-DUMPBED " << (int) memory[I] << "-" << (int) memory[I+1] << "-" << (int) memory[I+2] <<std::endl; 
        }
        break;
        case 0x0029: I = V[arg]*5; break;
        case 0x0055:
        {
          for(int i = 0; i < arg; i++)
            memory[I+i] = V[i];
        }
        break;
        case 0x0065:
        {
          for(int i = 0; i < arg; i++)
            V[i] = memory[I+i];
        }
        break;
        default: LOG("UNKNOWN INSTRUCTION!" << int_to_string(op)); break;
      }

      pc += 2;
    }
    break;
    default:
      printf("Unknown opcode 0x%X\n", op);
      break;
  }
  
  if(delay_timer > 0)
  {
    --delay_timer;
    if(delay_timer == 1)
      printf("D: timer!\n");
  }
  
  if(sound_timer > 0)
  {
    if(sound_timer == 1){
      gGame.beep();
      printf("BEEP!\n");
    }
    --sound_timer;
  }  
}

