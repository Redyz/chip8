#include "game.hpp"
#include <SDL2/SDL.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_mixer.h>
#include "cpu.hpp"

// 0x000-0x1FF - Chip 8 interpreter (contains font set in emu)
// 0x050-0x0A0 - Used for the built in 4x5 pixel font set (0-F)
// 0x200-0xFFF - Program ROM and work RAM
void Game::setup()
{
  if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
  {
    printf("ERROR: %s\n", SDL_GetError());
  }
  else
  {
    window = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, S_WIDTH+10, S_HEIGHT+10, SDL_WINDOW_SHOWN );
    if( window == NULL )
    {
      printf( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
    }
    screen_surface = SDL_GetWindowSurface( window );
  }
  
  if( Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1 )
  {
  }
  snd_boop = Mix_LoadWAV("boop.wav");
  
  cpu.initialize();
}

void Game::blank()
{
   SDL_FillRect( screen_surface, NULL, SDL_MapRGB( screen_surface->format, 0xFF, 0xFF, 0xFF ) );
}

void Game::beep()
{
  Mix_PlayChannel( -1, snd_boop, 0 );
}

void Game::clear()
{
//   SDL_FillRect( screen_surface, NULL, SDL_MapRGB( screen_surface->format, 0xFF, 0xFF, 0xFF ) );
  for(int i = 0; i < HEIGHT*WIDTH; i++)
  {
    cpu.gfx[i] = 0;
  }
}

void Game::input()
{
  SDL_Event event;
  while (SDL_PollEvent(&event)) 
  {
    if(event.type == SDL_QUIT || (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE))
    {
      running = false;
    }
    
    bool isUp = false, pressed = false;
    switch(event.type)
    {
      case SDL_KEYDOWN:
        isUp = false;
        pressed = true;
        break;
      case SDL_KEYUP:
        isUp = true;
        pressed = true;
        break;
    }
    if(pressed)
    {
      int code = 0;
      switch(event.key.keysym.scancode)
      {
        case SDL_SCANCODE_1: code = 0; break;
        case SDL_SCANCODE_2: code = 1; break;
        case SDL_SCANCODE_3: code = 2; break;
        case SDL_SCANCODE_4: code = 3; break;
        case SDL_SCANCODE_Q: code = 4; break;
        case SDL_SCANCODE_W: code = 5; break;
        case SDL_SCANCODE_E: code = 6; break;
        case SDL_SCANCODE_R: code = 7; break;
        case SDL_SCANCODE_A: code = 8; break;
        case SDL_SCANCODE_S: code = 9; break;
        case SDL_SCANCODE_D: code = 10; break;
        case SDL_SCANCODE_F: code = 11; break;
        case SDL_SCANCODE_Z: code = 12; break;
        case SDL_SCANCODE_X: code = 13; break;
        case SDL_SCANCODE_C: code = 14; break;
        case SDL_SCANCODE_V: code = 15; break;
        default: code = -1; break;
      }
      
      if(code >= 0)
      {
        cpu.key[code] = !isUp;
      }
    }
  }
}

void Game::destroy()
{
  SDL_DestroyWindow( window );
  
  Mix_CloseAudio();
  
  //Quit SDL subsystems
  SDL_Quit(); 
}

void Game::draw_gfx()
{
//   LOG("DRAWING GFX");
//   LOG("===========");
  for(int i = 0; i < HEIGHT*WIDTH; i++)
  {
    int x = i % WIDTH;
    int y = i / WIDTH;
    unsigned char& current = cpu.gfx[i];
//     std::cout << (int) current << " ";
    if(current != 0)
      draw_rectangle(x, y);
    
//     if(x == WIDTH-1)
//       std::cout << std::endl;
  }
  
  cpu.draw_flag = false;
  SDL_UpdateWindowSurface( window );
}

void Game::draw_rectangle(int ax, int ay)
{
  int scale = SCALE;
  SDL_Rect r;
  r.x = ax*scale;
  r.y = ay*scale;
  r.w = 1*scale;
  r.h = 1*scale;
  
// 	printf("D %i:%i --> %i:%i \n", r.x, r.y, r.w, r.h);
  
  SDL_FillRect(screen_surface, &r, SDL_MapRGB(screen_surface->format, 0x00, 0x00, 0x00));
}
